# Simple linux infrastructure

## Playbooks

Supported only CentOS/Oracle/Debian/Ubuntu distros.
The infrastructure is managed by the following playbooks.

#### bootstrap.yml

Gets the value of the `python` interpreter or installs the interpreter into the system.
Supports package managers only ** 'apt' ** and ** 'yum' **.

#### clear_docker.yml

Required to run only on the hosts where Docker is located. It clears launched containers,
but not destroyed after the startup files.


#### update.yml

Starts the installation process for base packages and system updates.
Does not manage configuration files, etc. Only installation and update.
Available variables:

*  __*system_packages*__ - packages to install. Default: see in **./group_vars/all.yml**.
*  __*update_packages*__ - update all system packages on run. Default: **false**.
*  __*enable_epel_release*__ - install `epel-release` repo for RH-hosts.
                               Default: **false**.

## Project settings

#### [defaults]

* forks - 4 threads
* roles_path - project_root/roles
* fact_caching_connection - /tmp/infrastructure_facts_cache
* fact_caching_timeout - 86400
* retry_files_enabled - no

#### [privilege_escalation]

* become_ask_pass - disabled.

#### [ssh_connection]

* ssh_args - disabled `StrictHostKeyChecking` and redirect `UserKnownHostsFile` to 
             `/dev/null`. Stay connected 10 min.

## Authors

[Sergey Klyuykov](sergey.k@vstconsulting.net)
